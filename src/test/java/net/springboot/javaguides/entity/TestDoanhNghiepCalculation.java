package net.springboot.javaguides.entity;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import junit.framework.TestCase;
import net.springboot.javaguides.entity.DoanhNghiep;
import net.springboot.javaguides.entity.NhanVien;

@SpringBootTest
public class TestDoanhNghiepCalculation  {
	//không có nhân viên
	@Test
	public void TestEmptyList() {
		List<NhanVien> list=new ArrayList<>();
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	//có một nhân viên nhưng lương và phụ cấp = 0
	@Test
	public void TestEmptyNhanvien() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien());
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	//có một nhân viên nhưng lương và phụ cấp = 0
	@Test
	public void Test_mucLuong_phuCap_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(0L,0L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	//có một nhân viên nhưng lương < 0
	@Test
	public void Test_mucLuong_nho_hon_0(){
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(-1L,1000000L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	//có một nhân viên nhưng phụ cấp < 0
	@Test
	public void Test_phuCap_nho_hon_0(){
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,-1L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(0, total);
	}
	
	//có một nhân viên nhưng lương = 0
	@Test
	public void Test_mucLuong_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(0L,10000000L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(450000, total);
	}
	
	//có một nhân viên nhưng phụ cấp = 0
	@Test
	public void Test_phuCap_bang_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(10000000L,0L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(450000, total);
	}
	
	//có một nhân viên nhưng lương và phụ cấp > 0
	@Test
	public void Test_mucLuong_phuCap_Lon_Hon_0() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,1000000L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(90000, total);
	}
	
	//có 2 nhân viên nhưng lương và phụ cấp > 0
	@Test
	public void Test_2_NhanVien() {
		List<NhanVien> list=new ArrayList<>();
		list.add(new NhanVien(1000000L,1000000L));
		list.add(new NhanVien(1000000L,1000000L));
		long total =new DoanhNghiep().CalCulate(list);
		Assert.assertEquals(180000, total);
	}
}
